package nl.harmwal.hypercube;

import java.applet.*;
import java.net.*;
import java.lang.reflect.*;
import java.awt.event.*;

public class Global{

    // The sounds are from gnome-audio-2.0.0
    //
    // http://ftp.acc.umu.se/pub/GNOME/sources/gnome-audio/2.0/
    //
    // I don't know who created these sounds, but there shouldn't be
    // any licensing issues.
    //
    // Converted from wav to au with:
    // sox infile.wav -r 8012 -U -b outfile.au

    static AudioClip bump;
    static AudioClip kick;
    static AudioClip bounce;
    static AudioClip pass;
    static AudioClip gotcha;
    static AudioClip hit;
    static AudioClip gameover;

    static Applet applet;

    static Method getMod;
    static int button2, button3;

    static void init(Applet appletParam) {

	applet = appletParam;
	
	System.out.println("This is nl.harmwal.hypercube " + Version.nr + "/" + Build.date + ".");
	System.out.println("Copyright (C) 1998, 2006 Harmen van der Wal.");
	System.out.println("This comes with ABSOLUTELY NO WARRANTY.");
	System.out.println("This is free software, and you are welcome to redistribute it under certain conditions.");
	System.out.println("See http://www.harmwal.nl/hypercube/ for details.");
	
	String bumpString = "sounds/info.au";
	String kickString = "sounds/clicked.au";
	String bounceString = "sounds/toggled.au";
	String passString = "sounds/activate.au";
	String gotchaString = "sounds/warning.au";
	String hitString = "sounds/email.au";
	String gameoverString = "sounds/shutdown1.au";

	URL url;
	Class cls = null;
	Class partypes[] = null;;

	try{

	    if (applet != null) {
		
		url = applet.getCodeBase();

		bump = applet.getAudioClip(url, bumpString);
		kick = applet.getAudioClip(url, kickString);
		bounce = applet.getAudioClip(url, bounceString);
		pass = applet.getAudioClip(url, passString);
		gotcha = applet.getAudioClip(url, gotchaString);
		hit = applet.getAudioClip(url, hitString);
		gameover = applet.getAudioClip(url, gameoverString);
		
	    }else{
		
		// Java 1.1 supports getAudioClip for applets,
		// but not newAudioClip for apps.
		// By using reflection, we don't break 1.1 compilers,
		// and SableVM 1.13 (Debian (etch) free-java-sdk).
		
		cls = Class.forName("java.applet.Applet");
		partypes = new Class[1];
		partypes[0] = Class.forName("java.net.URL");
		Method method = cls.getMethod("newAudioClip", partypes);
		Object[] arglist = new Object[1];

		url = new URL(new String("file:" + bumpString));
		arglist[0] = (Object)url;
		bump = (AudioClip)method.invoke(new Applet(), arglist);

		url = new URL(new String("file:" + kickString));
		arglist[0] = (Object)url;
		kick = (AudioClip)method.invoke(new Applet(), arglist);


		url = new URL(new String("file:" + bounceString));
		arglist[0] = (Object)url;
		bounce = (AudioClip)method.invoke(new Applet(), arglist);


 		url = new URL(new String("file:" + passString));
		arglist[0] = (Object)url;
		pass = (AudioClip)method.invoke(new Applet(), arglist);

		url = new URL(new String("file:" + gotchaString));
		arglist[0] = (Object)url;
		gotcha = (AudioClip)method.invoke(new Applet(), arglist);

		url = new URL(new String("file:" + hitString));
		arglist[0] = (Object)url;
		hit = (AudioClip)method.invoke(new Applet(), arglist);

		url = new URL(new String("file:" + gameoverString));
		arglist[0] = (Object)url;
		gameover = (AudioClip)method.invoke(new Applet(), arglist);

	    }
	}catch(Exception e){
	    e.printStackTrace();
	}

	// getModifier doesn't give the right mousebutton for mousedrags,
	// at least with SableVM version 1.13 (Debian free-java-sdk).
	// A workaround is to use getModifiersEx, but this requires 
	// Java version >= 1.4, so we user reflection again.
	try {
	    cls = Class.forName("java.awt.event.InputEvent");
	    partypes = new Class[0];
	    getMod = cls.getMethod("getModifiersEx", partypes);
	    button2 = InputEvent.BUTTON2_DOWN_MASK;
	    button3 = InputEvent.BUTTON3_DOWN_MASK;
	    
	}catch(Exception e){
	    e.printStackTrace();
	    try {
		getMod = cls.getMethod("getModifiers", partypes);
		button2 = InputEvent.BUTTON2_MASK;
		button3 = InputEvent.BUTTON3_MASK;
	    }catch(Exception e2){
		e2.printStackTrace();
	    }
	}
    }
}
