package nl.harmwal.hypercube;

import java.applet.*;

public class Main extends Applet{

    static Applet applet;
    static Space space;

    public static void main(String[] args) {

	String[] p = new String[4];
	p[0] = "4";
	p[1] = "normal";
	p[2] = "false";
	p[3] = "false";

	for (int arg = 0; arg < 4; arg++) {
	    if (args.length > arg) {
		p[arg] = args[arg];
	    }
	}
	initialize(p[0], p[1], p[2], p[3]);
    }

     public void init() {

	 applet = this;

	 initialize(getParameter("dimensions"),
		    getParameter("stereoscopic"),
		    getParameter("swap"),
		    getParameter("sound"));
     }

    private static void initialize(String dimensionsString, 
				   String stereoString,
				   String swapString,
				   String soundString) {

	int dimensions = Integer.parseInt(dimensionsString);
	boolean sound = Boolean.valueOf(soundString).booleanValue();
	boolean swap = Boolean.valueOf(swapString).booleanValue();

	Global.init(applet);
	space = new Space(dimensions,
			  stereoString,
			  swap,
			  sound,
			  applet);
    }

    public void start() {
	//System.out.println("Start");
	space.running = true;
	space.pauzed = false;
	//space.focus = true;
	space.resume();
    }

    public void stop() {
	//System.out.println("Stop");
	space.pauzed = true;
    }

    public void destroy() {
	//System.out.println("Destroy");
	space.running = false;;
	space.resume();
	space = null;
    }
}
