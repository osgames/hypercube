package nl.harmwal.hypercube;

import java.applet.*;
import java.awt.*;
import java.awt.event.*;

public class Launcher extends Applet implements ActionListener
{

    Dimension offDimension;
    Image offImage;
    Graphics offGraphics;

    Panel panel = new Panel();

    Button button2d = new Button("2d");
    Button button3d = new Button("3d");
    Button button4d = new Button("4d");

     public void init() {

	 Global.init(this);

	 panel.setLayout(new GridLayout(3, 0));
	 panel.add(button4d);
	 panel.add(button3d);
	 panel.add(button2d);
	 add(panel);

	 setLayout(new BorderLayout());
	 add("South", panel);

	button2d.addActionListener(this);
	button3d.addActionListener(this);
	button4d.addActionListener(this);
     }


    public void actionPerformed(ActionEvent e) {
        if ("2d".equals(e.getActionCommand())) {
	    new Space(2, "normal", false, false, null);
        }
        if ("3d".equals(e.getActionCommand())) {
	    new Space(3, "cross eyed", false, false, null);
        }
        if ("4d".equals(e.getActionCommand())) {
	    new Space(4, "cross eyed", false, false, null);
        }

    }

    public void paint(Graphics graphics) {
	update(graphics);
    }

    public void update(Graphics graphics) {

	Dimension dimension = getSize();
	
	// Create the offscreen graphics context, if no good one exists.
	if ((offGraphics == null) || 
	    (dimension.width != offDimension.width) || 
	    (dimension.height != offDimension.height)) {
	    offDimension = dimension;
	    offImage = createImage(dimension.width, dimension.height);
	    offGraphics = offImage.getGraphics();
	}
	
	offGraphics.setColor(Color.white);
	offGraphics.fillRect(0, 0, dimension.width, dimension.height);

	offGraphics.setColor(Color.black);

	double angle = 0;

	int size=30;
	int x1 = 35;
	int y1 = 20;

	int x2, y2;
	
	for (int t = 1; t < 9; t++){
	    for (int t2 = 1; t2 < 6; t2++){

		x2 = x1 + (int)(Math.round(size * Math.cos(angle)));
		y2 = y1 + (int)(Math.round(size * Math.sin(angle)));

		offGraphics.drawLine(x1 , y1, x2, y2);
	
		if (t2 < 5){
		    angle += .5 * Math.PI;
		}else{
		    angle += .25 * Math.PI;
		}
		
		x1 = x2;
		y1 = y2;
		
	    }
	}
	graphics.drawImage(offImage, 0, 0, this);

    }
}
